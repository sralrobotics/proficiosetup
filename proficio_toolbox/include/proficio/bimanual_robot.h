/*
 * Copyright 2016 Barrett Technology <support@barrett.com>
 *
 * This file is part of proficio_toolbox.
 *
 * This version of proficio_toolbox is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This version of proficio_toolbox is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this version of proficio_toolbox. If not, see <http://www.gnu.org/licenses/>.
 */

/** @file bimanual_robot.h
 *
 * Defines a standard class for running two robots on a single computer and
 * provides systems for communication between the two robots.
 *
 * The robots will be configured for pendantless operation and started up
 * automatically if the NO_CONTROL_PENDANT macro is defined.
 */

// @TODO(ab): Test what happens if someone tries to use this in a program with
// a proficio_main function. This should be incompatible, and it will probably
// fail in interesting ways.

// @TODO(ab): Add WAM compatibility.

// @TODO(ab): Add ability to restart after quitting.

#ifndef PROFICIO_BIMANUAL_ROBOT_H_
#define PROFICIO_BIMANUAL_ROBOT_H_

#include <iostream>
#include <string>

#include <boost/function.hpp>                        // NOLINT(build/include_order)
#include <boost/thread.hpp>                          // NOLINT(build/include_order)

#include <barrett/config.h>                          // NOLINT(build/include_order)
#include <barrett/exception.h>                       // NOLINT(build/include_order)
#include <barrett/log.h>                             // NOLINT(build/include_order)
#include <barrett/products/product_manager.h>        // NOLINT(build/include_order)
#include <barrett/systems.h>                         // NOLINT(build/include_order)
#include <barrett/units.h>                           // NOLINT(build/include_order)

#include <proficio/tools/configure_safety_module.h>  // NOLINT(build/include_order)

#ifdef NO_CONTROL_PENDANT
#  define BARRETT_SMF_CONFIGURE_PM
#  define BARRETT_SMF_DONT_PROMPT_ON_ZEROING
#  define BARRETT_SMF_DONT_WAIT_FOR_SHIFT_ACTIVATE
#endif

#ifndef BARRETT_SMF_DONT_WAIT_FOR_SHIFT_ACTIVATE
#  define BARRETT_SMF_WAIT_FOR_SHIFT_ACTIVATE true
#else
#  define BARRETT_SMF_WAIT_FOR_SHIFT_ACTIVATE false
#endif

#ifndef BARRETT_SMF_DONT_PROMPT_ON_ZEROING
#  define BARRETT_SMF_PROMPT_ON_ZEROING true
#else
#  define BARRETT_SMF_PROMPT_ON_ZEROING false
#endif

namespace proficio {

/** Runs two robots and provides systems for communication between the two
 * robots. The communication is provided via ExposedOutput systems that can be
 * accessed by the program using this class. The systems provide joint
 * positions, tool position, and joint velocities that are set every 1 ms.
 *
 * The robots will be configured for pendantless operation and started up
 * automatically if the NO_CONTROL_PENDANT macro is defined.
 *
 * For an example of use, @see proficio_demos/master_master/master_master.cpp
 */
template <size_t DOF>
class BimanualRobot {
  BARRETT_UNITS_TEMPLATE_TYPEDEFS(DOF);

 public:
  /** Constructor. No work needs to be done here. It is all done in init()
   * instead. Initialize the ready, done, and qutting flags to false.
   */
  BimanualRobot()
    : product_manager_1_(NULL), product_manager_2_(NULL),
      ready_1_(false), ready_2_(false),
      done_1_(false), done_2_(false),
      quitting_(false), initialized_(false) {}

  ~BimanualRobot();
  bool init(std::string, std::string);

  /** Returns a pointer to the ProductManager object corresponding to the
   * robot number. Returns NULL for invalid robot numbers.
   *
   * @param robot_number    The number of the robot (1 or 2)
   */
  barrett::ProductManager* getPm(int robot_number) {
    switch (robot_number) {
      case 1:
        return product_manager_1_;
      case 2:
        return product_manager_2_;
      default:
        return NULL;
    }
  }

  /** Returns a pointer to the Wam object corresponding to the robot number.
   * Returns NULL for invalid robot numbers.
   * @param robot_number    The number of the robot (1 or 2)
   */
  barrett::systems::Wam<DOF>* getWam(int robot_number) {
    switch (robot_number) {
      case 1:
        return product_manager_1_->getWam3();
      case 2:
        return product_manager_2_->getWam3();
      default:
        return NULL;
    }
  }

  /** Returns a pointer to an ExposedOutput system containing joint positions
   * of the robot. This can be connected with systems that are managed by the
   * other robot's execution manager. Returns NULL for invalid robot numbers.
   *
   * @param robot_number    The number of the robot (1 or 2)
   */
  barrett::systems::ExposedOutput<jp_type>* getJointPosSys(int robot_number) {
    switch (robot_number) {
      case 1:
        return &joint_position_sys_1_;
      case 2:
        return &joint_position_sys_2_;
      default:
        return NULL;
    }
  }

  /** Returns a pointer to an ExposedOutput system containing the tool position
   * of the robot. This can be connected with systems that are managed by the
   * other robot's execution manager. Returns NULL for invalid robot numbers.
   *
   * @param robot_number    The number of the robot (1 or 2)
   */
  barrett::systems::ExposedOutput<cp_type>* getToolPosSys(int robot_number) {
    switch (robot_number) {
      case 1:
        return &tool_position_sys_1_;
      case 2:
        return &tool_position_sys_2_;
      default:
        return NULL;
    }
  }

  /** Returns a pointer to an ExposedOutput system containing joint velocities
   * of the robot. This can be connected with systems that are managed by the
   * other robot's execution manager. Returns NULL for invalid robot numbers.
   *
   * @param robot_number    The number of the robot (1 or 2)
   */
  barrett::systems::ExposedOutput<jv_type>* getJointVelSys(int robot_number) {
    switch (robot_number) {
      case 1:
        return &joint_velocity_sys_1_;
      case 2:
        return &joint_velocity_sys_2_;
      default:
        return NULL;
    }
  }

  /** Indicates whether the robot is initialized or not. When the robot is in
   * ACTIVE mode with all of its output systems set up, this will return true.
   *
   * @param robot_number    The number of the robot
   */
  bool isReady(int robot_number) {
    switch (robot_number) {
      case 1:
        return ready_1_;
      case 2:
        return ready_2_;
      default:
        return false;
    }
  }

  /** Indicates whether the robot has completed its shut down sequence or not.
   * When this returns true, the robot will be in IDLE mode.
   *
   * @param robot_number    The number of the robot
   */
  bool isShutdownComplete(int robot_number) {
    switch (robot_number) {
      case 1:
        return done_1_;
      case 2:
        return done_2_;
      default:
        return false;
    }
  }

  /** Sends a quit signal to both robots so they will begin their shutdown
   * sequence.
   */
  void quit() { quitting_ = true; }

  /** Returns true if the robot shutdown sequence is in progress. */
  bool isQuitting() { return quitting_; }

 private:
  /** Sets a flag indicating the 'ready' state of the robot. True indicates that
   * the robot is initialized and ready for use.
   *
   * @param robot_number    The number of the robot
   * @param ready           The ready flag value to set
   */
  void setReady(int robot_number, bool ready) {
    switch (robot_number) {
      case 1:
        ready_1_ = ready;
        break;
      case 2:
        ready_2_ = ready;
        break;
    }
  }

  /** Sets a flag indicating the 'done' state of the robot. True indicates that
   * the robot has completed its shutdown sequence and is in IDLE mode.
   *
   * @param robot_number    The number of the robot
   * @param done            The done flag value to set
   */
  void setShutdownComplete(int robot_number, bool done) {
    switch (robot_number) {
      case 1:
        done_1_ = done;
        break;
      case 2:
        done_2_ = done;
        break;
    }
  }

  /** Sends a quit signal and sets this robot's state as done. This function is
   * called when an unsupported robot is detected. It is not called when no
   * robot is connected, because this situation is handled earlier.
   *
   * @param bimanual_robot       Pointer to the BimanualRobot object
   * @param robot_number         Robot number (currently 1 or 2)
   */
  void unsupportedRobotThread(BimanualRobot<DOF>* const bimanual_robot,
                           int robot_number);

  /** Finishes the robot startup sequence and starts the main loop, which keeps
   * track of robot state, sets the outputs of the systems, and checks for quit
   * signals.
   *
   * @param product_manager      Pointer to the product manager for this robot
   * @param robot                The Wam object
   * @param bimanual_robot       Pointer to the BimanualRobot object
   * @param robot_number         Robot number (currently 1 or 2)
   */
  void robotThread(barrett::ProductManager* product_manager,
                   const barrett::systems::Wam<DOF>& robot,
                   BimanualRobot<DOF>* const bimanual_robot,
                   int robot_number);

  boost::thread startRobot(barrett::ProductManager* product_manager,
                           BimanualRobot<DOF>* const bimanual_robot,
                           int robot_number);

#ifdef BARRETT_SMF_CONFIGURE_PM
  bool configureProductManager(barrett::ProductManager* product_manager);
#endif

  barrett::ProductManager *product_manager_1_, *product_manager_2_;
  barrett::systems::ExposedOutput<jp_type> joint_position_sys_1_,
                                           joint_position_sys_2_;
  barrett::systems::ExposedOutput<cp_type> tool_position_sys_1_,
                                           tool_position_sys_2_;
  barrett::systems::ExposedOutput<jv_type> joint_velocity_sys_1_,
                                           joint_velocity_sys_2_;
  bool ready_1_, ready_2_, done_1_, done_2_, quitting_, initialized_;
  boost::thread robot_thread_1_, robot_thread_2_;
};

/** Destructor. Cleans up memory allocated in init(). */
template <size_t DOF>
BimanualRobot<DOF>::~BimanualRobot() {
  if (product_manager_1_ != NULL) {
    delete product_manager_1_;
  }
  if (product_manager_2_ != NULL) {
    delete product_manager_2_;
  }
}

/** Creates product managers for both robots and starts up the robot control
 * threads, which will activate the robots. Requires locations of configuration
 * files, which indicate the configuration (right or left) and port (0 or 1),
 * e.g., "proficio_left/default.conf.0".
 *
 * @param conf1           Configuration file for robot 1
 * @param conf2           Configuration file for robot 2
 */
template <size_t DOF>
bool BimanualRobot<DOF>::init(std::string conf1, std::string conf2) {
  if (initialized_) {
    // Don't allow multiple calls of init() because it will crash the progam.
    return false;
  } else {
    initialized_ = true;

    // Gives us pretty stack traces if things fail.
    barrett::installExceptionHandler();

    // Create a product manager for each robot with the specified configuration
    // file. EtcPathRelative looks for the files in ~/.barrett/ first, and if
    // that folder does not exist it checks /etc/barrett/.
    //
    // @TODO(ab): Fix program crash when no robot is connected.
    // Note that the program will crash here if either robot is not connected
    // because the ProductManager constructor results in an error thrown. See
    // libbarrett/include/barrett/products/detail/puck-inl.h, line 97.
    product_manager_1_ =
      new barrett::ProductManager(barrett::EtcPathRelative(conf1).c_str());
    product_manager_2_ =
      new barrett::ProductManager(barrett::EtcPathRelative(conf2).c_str());

    // Start up each individual arm
    int robot_number = 1;
    printf("Starting robot thread %d...\n", robot_number);
    robot_thread_1_ = startRobot(product_manager_1_, this, robot_number);
    robot_number = 2;
    printf("Starting robot thread %d...\n", robot_number);
    robot_thread_2_ = startRobot(product_manager_2_, this, robot_number);
    return true;
  }
}

/** Configures the product manager for pendantless operation if applicable,
 * wakes up the robot and the pucks, and starts the robot thread. If no
 * Proficio is found, exit.
 *
 * @param product_manager      Pointer to the product manager for this robot
 * @param bimanual_robot       Pointer to the BimanualRobot object
 * @param robot_number         Robot number (currently 1 or 2)
 * @param robot_thread         Function that will be run as the robot thread
 *
 * @TODO(ab): Test WAM compatibility. Should already be compatible.
 */
template <size_t DOF>
boost::thread BimanualRobot<DOF>::startRobot(
    barrett::ProductManager* product_manager,
    BimanualRobot<DOF>* const bimanual_robot,
    int robot_number) {
#ifdef BARRETT_SMF_CONFIGURE_PM
  bimanual_robot->configureProductManager(product_manager);
#endif

#ifndef NO_CONTROL_PENDANT
  product_manager->waitForWam(BARRETT_SMF_PROMPT_ON_ZEROING);
#endif

  product_manager->wakeAllPucks();

  if (product_manager->foundWam3()) {
    return boost::thread(boost::bind(&BimanualRobot::robotThread, this,
                                     product_manager,
                                     boost::ref(*product_manager->getWam3(
                                         BARRETT_SMF_WAIT_FOR_SHIFT_ACTIVATE)),
                                     bimanual_robot,
                                     robot_number));
  } else {
    return boost::thread(boost::bind(&BimanualRobot::unsupportedRobotThread,
                                     this, bimanual_robot, robot_number));
  }
}

/* This will only be called if there is a robot connected by it is not
 * supported by this program. If there is no robot connected, the instantiation
 * of ProductManager will have failed before this function is called.
 */
template <size_t DOF>
void BimanualRobot<DOF>::unsupportedRobotThread(
    BimanualRobot<DOF>* const bimanual_robot,
    int robot_number) {
  printf("ERROR: Robot %d type not supported. Quitting.\n", robot_number);
  bimanual_robot->quit();
  bimanual_robot->setShutdownComplete(robot_number, true);
  // Ok to return at this point. The other robot will quit when it is able.
}

/* Finishes the robot startup sequence, sets the initial outputs of the
 * systems, and starts the main loop. In the main loop: check the pendant state,
 * set the exposed output values (joint positions, tool position, joint
 * velocities), and check for quit signals. Shut down the robot if a quit signal
 * is received.
 *
 * @TODO(ab): Update for WAM compatibility
 */
template <size_t DOF>
void BimanualRobot<DOF>::robotThread(barrett::ProductManager* product_manager,
                                     const barrett::systems::Wam<DOF>& robot,
                                     BimanualRobot<DOF>* const bimanual_robot,
                                     int robot_number) {
  BARRETT_UNITS_TEMPLATE_TYPEDEFS(DOF);

  // TODO(ab): Move robot startup and systems connect to startRobot function.
  // robotThread should only contain the loop that sets values and checks for
  // quit. Make a connectSystems function that checks whether connects are
  // successful.
  barrett::SafetyModule* safety_module = product_manager->getSafetyModule();

#ifdef NO_CONTROL_PENDANT
  // TODO(ab): The for loop and cout statements here were an effort to debug
  // the startup sequence. Sometimes the safety module doesn't go to ACTIVE
  // mode automatically. Debugging has failed so far because the problem goes
  // away when these cout statements are present... Timing issue?
  std::cout << "starting robot thread..." << std::flush;
  for (int i = 0; i < 3; i++) {
    if (safety_module->getMode() != barrett::SafetyModule::ACTIVE) {
      std::cout << " " << i << std::flush;
      barrett::systems::ExposedOutput<jt_type> zero_torque_sys;
      jt_type zero_torque(0.0);
      zero_torque_sys.setValue(zero_torque);
      barrett::systems::connect(zero_torque_sys.output, robot.input);
      usleep(50000);
      safety_module->setMode(barrett::SafetyModule::ACTIVE);
      if (i >= 2) {
        safety_module->waitForMode(barrett::SafetyModule::ACTIVE, false);
      } else {
        usleep(200000);
      }
      barrett::systems::disconnect(robot.input);
    }
  }
  std::cout << std::endl;
  robot.getLowLevelWam().getPuckGroup().setProperty(
      barrett::Puck::MODE, barrett::MotorPuck::MODE_TORQUE);
#endif

  // Get pointers to ExposedOutput systems for this robot
  barrett::systems::ExposedOutput<jp_type> *joint_pos =
    bimanual_robot->getJointPosSys(robot_number);
  barrett::systems::ExposedOutput<cp_type> *tool_pos =
    bimanual_robot->getToolPosSys(robot_number);
  barrett::systems::ExposedOutput<jv_type> *joint_vel =
    bimanual_robot->getJointVelSys(robot_number);

  // Set initial values of ExposedOutput systems
  joint_pos->setValue(robot.getJointPositions());
  tool_pos->setValue(robot.getToolPosition());
  joint_vel->setValue(robot.getJointVelocities());

  // Associate this robot's ExposedOutput systems with other robot's product
  // manager. This ensures that the user of this class is accessing the
  // appropriate combinations of "this robot" and "other robot" when they start
  // connecting systems in their main function. Robot numbers are 1 and 2, so
  // other robot number is 3 - robot_number. Note that if a user connects
  // systems to the wrong execution manager the code will still compile, but it
  // will throw a run-time error as soon as the systems are connected.
  //
  // TODO(ab): Is there a way to check for connection to the correct execution
  // manager at compile time? This would be a change in libbarrett, not here.
  barrett::ProductManager* product_managerOther =
    bimanual_robot->getPm(3 - robot_number);
  product_managerOther->getExecutionManager()->startManaging(*joint_pos);
  product_managerOther->getExecutionManager()->startManaging(*tool_pos);
  product_managerOther->getExecutionManager()->startManaging(*joint_vel);

  // Set the ready flag to true so the program using this class can know that
  // initialization is complete.
  bimanual_robot->setReady(robot_number, true);

  // Loop: As long as the robot is active, set the values of the exposed outputs
  // and check for quit signals from outside. If a quit signal has been
  // received, shut down the robot.
  while ((safety_module->getMode() == barrett::SafetyModule::ACTIVE) &&
         !bimanual_robot->isQuitting()) {
    joint_pos->setValue(robot.getJointPositions());
    tool_pos->setValue(robot.getToolPosition());
    joint_vel->setValue(robot.getJointVelocities());
    barrett::btsleepRT(0.001);  // btsleepRT is friendly to the real-time loop
  }

  safety_module->setMode(barrett::SafetyModule::IDLE);
  safety_module->waitForMode(barrett::SafetyModule::IDLE);

  // After shutting down the robot, set the done flag to true so the program
  // using this class can know that shutdown is complete.
  bimanual_robot->setShutdownComplete(robot_number, true);
}

/** Sets up the safety module for pendantless operation and ensures that it is
 * in the correct state to start the robot.
 *
 * @param product_manager      Pointer to the product manager to configure
 */
#ifdef BARRETT_SMF_CONFIGURE_PM
template <size_t DOF>
bool BimanualRobot<DOF>::configureProductManager(
    barrett::ProductManager* product_manager) {
  barrett::SafetyModule* safety_module = product_manager->getSafetyModule();
  configureSafetyModule(safety_module);
  barrett::SafetyModule::PendantState ps;

  safety_module->getPendantState(&ps);
  if (ps.pressedButton == barrett::SafetyModule::PendantState::ESTOP) {
    printf("Please release the E-Stop to start the robot.\n");
  }
  while (ps.pressedButton == barrett::SafetyModule::PendantState::ESTOP) {
    safety_module->getPendantState(&ps);
    barrett::btsleep(0.1);  // time is in seconds
  }
  if (safety_module->getMode() == barrett::SafetyModule::ESTOP ||
      safety_module->getMode() == barrett::SafetyModule::IDLE) {
    // Even if the SafetyModule is already in IDLE mode, set it to IDLE again
    // because this tells the SafetyModule to send some commands to the motor
    // pucks.
    safety_module->setMode(barrett::SafetyModule::IDLE);
    // Second argument is false to suppress printing the "Please shift-idle
    // the robot" message.
    safety_module->waitForMode(barrett::SafetyModule::IDLE, false);
  }
  return true;
}
#endif

}  // namespace proficio

#endif  // PROFICIO_BIMANUAL_ROBOT_H_
