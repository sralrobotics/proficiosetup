/* Copyright 2016 Barrett Technology <support@barrett.com>
 *
 * This file is part of proficio_toolbox.
 *
 * This version of proficio_toolbox is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This version of proficio_toolbox is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this version of proficio_toolbox.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

/** @file bimanual_haptic_bubble.h
 * Given a separation distance between two robots in a bimanual system,
 * create haptic "bubbles" around the endpoints to prevent collision.
 * This could be used to place a bubble around another object as well,
 * as long as the position is specified relative to the correct origin
 * (assumed to be the origin of another robot with opposite configuration).
 * 
 * This system applies forces to one robot only. To put haptic bubbles
 * around both robots, two systems are needed.
 * 
 * I/O        | Description
 * -----------|----------------------------------------------------------------
 * inputThis  | Cartesian position of the robot to be controlled by this system
 * inputOther | Cartesian position of the other robot
 * output     | Cartesian force applied to the controlled robot
 *
 * @param separation_distance  Translation (meters) between the two robot
 *                             centers. (Note that the center of the base is not
 *                             the origin of the world frame. See Kinematics doc
 *                             for details.)
 * @param bubble_radius        Distance (meters) at which repulsive forces start
 *                             to be applied (measured between robot endpoint
 *                             centers).
 * @param world_this           A pointer to a config_setting_t for this robot
 *                             (type defined in libconfig.h)
 * @param world_other          A pointer to a config_setting_t for the other
 *                             robot
 * @param stiffness            Stiffness of the haptic bubble, default 500 N/m
 * @param sys_name             Name of the system
 *
 * Note that inputThis and output can be connected directly to the 
 * controlled robot, but inputOther cannot be connected directly to the
 * other robot, since it is supervised by a different product manager. It
 * is assumed that the user of this system has created some thread-safe 
 * method of getting the other robot's position and (for example) assigning 
 * it to the value of an ExposedOutput system supervised by the same product 
 * manager as the controlled robot. Such a structure is implemented in the
 * BimanualRobot class, @see bimanual_robot.h.
 */

#ifndef PROFICIO_SYSTEMS_UTILITIES_BIMANUAL_HAPTIC_BUBBLE_H_
#define PROFICIO_SYSTEMS_UTILITIES_BIMANUAL_HAPTIC_BUBBLE_H_

#include <string>

#include <barrett/systems.h>                    // NOLINT(build/include_order)
#include <barrett/systems/abstract/system.h>    // NOLINT(build/include_order)
#include <barrett/units.h>                      // NOLINT(build/include_order)

namespace proficio {
namespace systems {

template<size_t DOF>
class BimanualHapticBubble : public barrett::systems::System {
  BARRETT_UNITS_TEMPLATE_TYPEDEFS(DOF);

 public:
  barrett::systems::System::Input<
    typename barrett::units::CartesianPosition::type> inputThis;
  barrett::systems::System::Input<
    typename barrett::units::CartesianPosition::type> inputOther;
  barrett::systems::System::Output<
    typename barrett::units::CartesianForce::type> output;

  /** Initializes the haptic bubble. Calculates the translation between the two
   * robot origins, which is needed to transform the other robot's position into
   * this robot's frame.
   */
  explicit BimanualHapticBubble(const cp_type separation_distance,
                                const double bubble_radius,
                                config_setting_t* world_this,
                                config_setting_t* world_other,
                                const double stiffness = 500,
                                const std::string& sys_name =
                                   "BimanualHapticBubble")
      : barrett::systems::System(sys_name),
        inputThis(this), inputOther(this), output(this, &outputValue),
        bubble_radius_(bubble_radius), output_data_(0.0), stiffness_(stiffness),
        active_(false) {
    // Get the translation from the world-to-base transforms in the config
    // files.
    config_setting_t * world_row;
    for (int i = 0; i < 3; ++i) {
      double translation_component_this, translation_component_other;
      world_row = config_setting_get_elem(world_this, i);
      bt_gsl_config_get_double(config_setting_get_elem(world_row, 3),
                               &translation_component_this);
      world_row = config_setting_get_elem(world_other, i);
      bt_gsl_config_get_double(config_setting_get_elem(world_row, 3),
                               &translation_component_other);
      translation_difference_[i] = translation_component_this -
                                   translation_component_other;
    }
    origin_offset_ = separation_distance - translation_difference_;
  }

  ~BimanualHapticBubble() { this->mandatoryCleanUp(); }

  /** Sets the stiffness of the haptic bubble wall. By default, this resets the
   * haptic bubble to its inactive state to prevent possibly large force
   * discontinuities. This can be overridden by passing 'true' for the second
   * argument. In this case, the user is responsible for not making sudden
   * changes in stiffness that might result in large force discontinuities.
   */
  void setStiffness(const double stiffness, const bool stay_active = false) {
    if (!stay_active) {
      active_ = false;
    }
    stiffness_ = stiffness;
  }

  /** Sets the separation distance between the two robots and updates related
   * parameters (origin offset). This should rarely be used, only when the two
   * robots are physically moved to a different separation distance. This resets
   * the haptic bubble to its initial inactive state. The wall reactivates when
   * the endpoint is in a position where the wall would not apply any force.
   */
  void setSeparationDistance(const cp_type separation_distance) {
    active_ = false;
    origin_offset_ = separation_distance - translation_difference_;
  }

  /** Sets the radius of the haptic bubbles (the distance between the two
   * endpoint centers for which force starts being applied). This is intended
   * to be used for swapping end effectors, if the new end effector is a
   * different size than the old one. This resets the haptic bubble to its
   * initial inactive state. The haptic bubble reactivates when the endpoint is
   * in a position where it would not apply any force.
   */
  void setRadius(const double radius) {
    active_ = false;
    bubble_radius_ = radius;
  }

 protected:
  typename barrett::systems::System::Output<
    typename barrett::units::CartesianForce::type>::Value* outputValue;
  cp_type origin_offset_;  ///< Translation (meters) from the other robot's
                           ///< world frame origin to this robot's world frame
                           ///< origin, specified in this robot's world frame.
  cp_type translation_difference_;  ///< Difference between the two robots'
                                    ///< world-to-base translations from the
                                    ///< config files, needed to calculate
                                    ///< origin_offset_ from separation_distance.
  double bubble_radius_;  ///< Distance (meters) at which repulsive forces
                          ///< start to be applied (measured between robot
                          ///< endpoint centers).
  cf_type output_data_;  // must be declared outside operate for output to work
  double stiffness_;
  bool active_;  ///< Flag indicating whether force is on or off; starts out
                 ///< false until output force would be zero to prevent sudden
                 ///< forces on startup.

  /** Applies forces if the robot endpoint has penetrated the haptic bubble
   * around the other robot endpoint. The haptic bubble starts out inactive
   * and activates the first time the force would be zero.
   */
  virtual void operate() {
    cp_type error = inputThis.getValue() -
                    (inputOther.getValue() - origin_offset_);
    double distance = error.norm();
    if (active_ && (distance < bubble_radius_)) {
      output_data_  = stiffness_ * (bubble_radius_ - distance) * (error / distance);
    } else if (!active_ && (distance >= bubble_radius_)) {
      active_ = true;
      output_data_.setZero();
    } else {
      output_data_.setZero();
    }
    this->outputValue->setData(&output_data_);
  }

 private:
  DISALLOW_COPY_AND_ASSIGN(BimanualHapticBubble);
};

}  // namespace systems
}  // namespace proficio

#endif  // PROFICIO_SYSTEMS_UTILITIES_BIMANUAL_HAPTIC_BUBBLE_H_
