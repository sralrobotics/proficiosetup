/* Copyright 2016 Barrett Technology <support@barrett.com>
 *
 * This file is part of proficio_toolbox.
 *
 * This version of proficio_toolbox is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This version of proficio_toolbox is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this version of proficio_toolbox.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

/** @file haptic_wall.h
 *
 * Create a haptic plane with stiffness and damping. Damping is directional, so
 * it only slows velocities directed into the plane. There are options for the
 * damping to only apply it over a certain speed and to ramp it up based on the
 * distance the cursor has penetrated into the object.
 * 
 * I/O            | Description
 * ---------------|------------------------------------------------------------
 * positionInput  | Cartesian position of the robot
 * velocityInput  | Cartesian velocity of the robot
 * forceOutput    | Cartesian force applied to the robot endpoint
 *
 * @param axis              A vector normal to the plane
 * @param point             A point on the plane (fully defines the plane when
 *                          combined with axis)
 * @param stiffness         Stiffness of the plane
 * @param damping           Damping of the plane
 * @param ramp_depth        Distance over which the damping ramps up
 * @param speed_cutoff      Speed above which the damping field takes effect
 * @param sys_name          Name of the system
 */

#ifndef PROFICIO_SYSTEMS_UTILITIES_HAPTIC_WALL_H_
#define PROFICIO_SYSTEMS_UTILITIES_HAPTIC_WALL_H_

#include <string>

#include <barrett/systems.h>                    // NOLINT(build/include_order)
#include <barrett/systems/abstract/system.h>    // NOLINT(build/include_order)
#include <barrett/units.h>                      // NOLINT(build/include_order)

namespace proficio {
namespace systems {

class HapticWall : public barrett::systems::System {
  BARRETT_UNITS_FIXED_SIZE_TYPEDEFS;

 public:
  barrett::systems::System::Input<
    typename barrett::units::CartesianPosition::type> positionInput;
  barrett::systems::System::Input<
    typename barrett::units::CartesianVelocity::type> velocityInput;
  barrett::systems::System::Output<
    typename barrett::units::CartesianForce::type> forceOutput;

  /** Initializes the parameters defining the haptic wall behavior. */
  explicit HapticWall(const cp_type& point,
                      const cp_type& axis,
                      const double stiffness = 0.0,
                      const double damping = 0.0,
                      const double ramp_depth = 0.0,
                      const double speed_cutoff = 0.0,
                      const std::string& sys_name = "HapticWall")
      : barrett::systems::System(sys_name),
        positionInput(this),
        velocityInput(this),
        forceOutput(this, &forceOutputValue),
        point_(point),
        stiffness_(stiffness),
        damping_(damping),
        damping_ramp_depth_(ramp_depth),
        speed_cutoff_(speed_cutoff),
        stiffness_active_(false),
        damping_active_(false) {
    normal_ = axis / axis.norm();  // normal_ must be a unit vector
    if (damping_ramp_depth_ < 0.0) {
      printf("Warning: Ramp depth cannot be negative (specified value was"
        "%f). Setting ramp depth to 0.\n", ramp_depth);
      damping_ramp_depth_ = 0.0;
    }
    if (speed_cutoff_ < 0.0) {
      printf("Warning: Speed cutoff cannot be negative (specified value was"
        "%f). Setting speed cutoff to 0.\n", speed_cutoff);
      speed_cutoff_ = 0.0;
    }
  }

  ~HapticWall() { this->mandatoryCleanUp(); }

  /** Checks whether the cursor is touching the wall. */
  bool isTouching(const cp_type& tool_pos) {
    // Depth of penetration into plane. depth > 0 means the cursor is touching.
    cp_type vec = point_ - tool_pos;
    double depth = dot(normal_, vec);
    return depth > 0;
  }

 protected:
  typename barrett::systems::System::Output<
    typename barrett::units::CartesianForce::type>::Value* forceOutputValue;

  cp_type point_, normal_;  ///< Point and normal defining the plane location
  double stiffness_, damping_;
  double damping_ramp_depth_;  ///< Depth over which to ramp up the damping force
                               ///< so it doesn't feel like a hard wall at high
                               ///< velocity.
  double speed_cutoff_;  ///< Cutoff speed over which to apply damping force.
  bool stiffness_active_;  ///< Flag indicating whether force is on or off.
                           ///< Starts out false until output force would be zero
                           ///< to prevent sudden forces on startup.
  bool damping_active_;  ///< Flag indicating whether force is on or off.
                         ///< Starts out false until output force would be zero
                         ///< to prevent sudden forces on startup.
  cf_type output_force_;  // must be persistent for output to work

  /** Dot product on two cp_type vectors. */
  double dot(cp_type vec1, cp_type vec2) {
    return ((vec1[0] * vec2[0]) + (vec1[1] * vec2[1]) + (vec1[2] * vec2[2]));
  }

  /** Calculate force applied by the haptic wall. The wall starts out inactive
   * and activates stiffness or damping the first time the force due to that
   * component is zero. Damping is only applied to velocity towards the center.
   * In the damping ramp zone, multiply the force by the percentage of
   * penetration into the ramp zone. This ramps up the damping force with depth
   * so a damping-only wall doesn't feel like a hard wall at high velocity.
   */
  cf_type calcForces(const cp_type& tool_pos, const cv_type& tool_vel) {
    // depth of penetration into plane
    double penetration_depth = dot(normal_, point_ - tool_pos);
    cf_type forces(0.0);
    if (penetration_depth > 0) {
      if (stiffness_active_) {
        forces = stiffness_ * penetration_depth * normal_;
      }

      cf_type damping_force(0.0);
      double speed_into_plane = -1.0 * dot(tool_vel, normal_);
      if (speed_into_plane > speed_cutoff_) {  // speed_cutoff_ is >= 0
        damping_force = damping_ * (speed_into_plane - speed_cutoff_) * normal_;
        if (penetration_depth < damping_ramp_depth_) {
          damping_force *= 1.0 -
            ((damping_ramp_depth_ - penetration_depth) / damping_ramp_depth_);
        }
        if (damping_active_) {
          forces += damping_force;
        }
      }
      if (!damping_active_ && (damping_force.norm() < 0.0001)) {
        damping_active_ = true;
      }
    } else {  // tool is outside the wall
      forces << 0, 0, 0;
      stiffness_active_ = true;
      damping_active_ = true;
    }
    return forces;
  }

  virtual void operate() {
    cp_type position = positionInput.getValue();
    cv_type velocity = velocityInput.getValue();
    output_force_ = calcForces(position, velocity);
    this->forceOutputValue->setData(&output_force_);
  }

 private:
  DISALLOW_COPY_AND_ASSIGN(HapticWall);
};

}  // namespace systems
}  // namespace proficio

#endif  // PROFICIO_SYSTEMS_UTILITIES_HAPTIC_WALL_H_
