# Proficio Toolbox

This is an extension to libbarrett that is specific to the Proficio.
It includes libbarrett-based systems, tools, and demos.
These are not guaranteed to run on any other robot other than Proficio.

For support, please email support@barrett.com.

## Pre-Requisite
* Install libbarrett from [here] (https://git.barrett.com/software/libbarrett). (Only runs with versions >= 1.2.4)
* sudo apt-get install libcgal-dev

## Build, compile and install
* cd ~/
* For users: git clone https://git.barrett.com/software/proficio_toolbox.git

    For developers: git clone git@git.barrett.com:software/proficio_toolbox.git
* cd proficio_toolbox
* mkdir build
* cd build
* cmake ../
* make
* sudo make install

## Programming

The programming is similar to that of libbarrett except for few minor changes.
* use 
```
  #include <proficio/systems/utilities.h>
```
* use 
```
  #include <proficio/standard_proficio_main.h> 
```
   instead of 
```
  #include <barrett/standard_main_function.h>
```
* use 
```
  template <size_t DOF>
  int proficio_main(int argc, char** argv, barrett::ProductManager& pm,
               barrett::systems::Wam<DOF>& wam, config side) {
```
   instead of 
```
  template <size_t DOF>
  int wam_main(int argc, char** argv, ProductManager& pm, systems::Wam<DOF>& wam) {
```
* There is also an option to skip the use of control pendant buttons (Shift activate and Shift idle).
***Please do not use this unless you know what you are doing. This can lead to undesirable safety issues.
It is highly recommended to remove this macro from all the demos that are shipped in this repository.*** Include
```
#define NO_CONTROL_PENDANT
```
before including
```
  #include <proficio/standard_proficio_main.h> 
```
In this case, you should zero the robot each time you power it on or change the configuration.
* To zero the robot, first move it to its home position and open joint 3 until the arm is fully extended.
Then open a terminal and run `homecheck <side>` where side is "left" or "right" based on the robot's configuration.
* For examples on programming and creating CMakeLists, please refer to the examples under proficio_demos.
