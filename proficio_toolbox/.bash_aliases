# for Proficio
alias leftConfig='mkdir -p ~/.barrett/proficio_left/calibration_data/wam3;
cp ~/proficio_toolbox/configurations/wam3.conf.left ~/.barrett/proficio_left/wam3.conf;  
cp ~/proficio_toolbox/configurations/zerocal.conf.left ~/.barrett/proficio_left/calibration_data/wam3/zerocal.conf; 
cp ~/proficio_toolbox/configurations/calibration.conf.left ~/.barrett/proficio_left/calibration.conf;
cp ~/proficio_toolbox/configurations/gravitycal.conf.left ~/.barrett/proficio_left/calibration_data/wam3/gravitycal.conf;
cp ~/proficio_toolbox/configurations/RightConfig.txt ~/.barrett/proficio_left/calibration_data/wam3/RightConfig.txt;
cp ~/proficio_toolbox/configurations/LeftConfig.txt ~/.barrett/proficio_left/calibration_data/wam3/LeftConfig.txt;
cp -r ~/.barrett/proficio_left/* ~/.barrett/'

alias rightConfig='mkdir -p ~/.barrett/proficio_right/calibration_data/wam3;
cp ~/proficio_toolbox/configurations/wam3.conf.right ~/.barrett/proficio_right/wam3.conf;  
cp ~/proficio_toolbox/configurations/zerocal.conf.right ~/.barrett/proficio_right/calibration_data/wam3/zerocal.conf; 
cp ~/proficio_toolbox/configurations/calibration.conf.right ~/.barrett/proficio_right/calibration.conf;
cp ~/proficio_toolbox/configurations/gravitycal.conf.right ~/.barrett/proficio_right/calibration_data/wam3/gravitycal.conf;
cp ~/proficio_toolbox/configurations/RightConfig.txt ~/.barrett/proficio_right/calibration_data/wam3/RightConfig.txt;
cp ~/proficio_toolbox/configurations/LeftConfig.txt ~/.barrett/proficio_right/calibration_data/wam3/LeftConfig.txt;
cp -r ~/.barrett/proficio_right/* ~/.barrett/'

alias checkSetup='head -n 1 ~/.barrett/wam3.conf; head -n 1 ~/.barrett/calibration.conf; head -n 1 ~/.barrett/calibration_data/wam3/zerocal.conf'
alias btutil='~/btclient/src/btutil/btutil'
alias p3util='~/btclient/src/p3util/p3util'
alias resetcan='sudo sh ~/btclient/reset_can.sh'
