/*  Copyright 2016 Barrett Technology <support@barrett.com>
 *
 *  This file is part of proficio_toolbox.
 *
 *  This version of proficio_toolbox is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This version of proficio_toolbox is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this version of proficio_toolbox.  If not, see
 *  <http://www.gnu.org/licenses/>.
 */

/** @file cube_sphere_bimanual.cpp
 *
 *  This is a demonstration of the capabilities of the Proficio robot for
 *  rehabilitation. In this demo, there is a box and sphere that provide haptic
 *  feedback when interacted with. The user can pop into and out of the objects
 *  if enough force is exerted. Two robots are used independently in this demo.
 *  There are no haptic interactions between the two robots.
 *
 *  In this demo, it is assumed that there is a left-configured Proficio
 *  connected to port 0 and a right-configured Proficio connected to port 1.
 *  It is recommended to run the bimanual_homecheck script before running this
 *  program to ensure that the robots are correctly connected and configured.
 *
 *  This file includes the haptics and logic behind the game. It must be run
 *  concurrently with the corresponding python visualization. To run the demo,
 *  run the following commands in two separate terminals:
 *
 *    ./cube_sphere_bimamual <IP address>
 *    python cube_sphere_bimanual_visualization.py <IP address>
 *
 *  By default, the IP address is 127.0.0.1 for both commands
 *
 *  User assistance is available in this demo. Gravity assist aids the user in
 *  supporting the weight of their own arm. The degree of assistance can be
 *  adjusted with the following key presses:
 *
 *  key          | action
 *  ------------ | -----------------------
 *  <up arrow>   | Increase gravity assist
 *  <down arrow> | Decrease gravity assist
 *  <delete>     | Turn off gravity assist
 *
 *  NOTICE: This program is for demonstration purposes only.
 *  It is not approved for clinical use.
 */

#include "cube_sphere_bimanual.h"               // NOLINT (build/include_subdir)

#include <string>
#include <signal.h>                             // NOLINT(build/include_order)

#include <boost/bind.hpp>                       // NOLINT(build/include_order)
#include <boost/tuple/tuple.hpp>                // NOLINT(build/include_order)

#include <barrett/config.h>                     // NOLINT(build/include_order)
#include <barrett/exception.h>                  // NOLINT(build/include_order)
#include <barrett/products/product_manager.h>   // NOLINT(build/include_order)
#include <barrett/systems.h>                    // NOLINT(build/include_order)
#include <proficio/systems/utilities.h>         // NOLINT(build/include_order)
#include <barrett/units.h>                      // NOLINT(build/include_order)

#define NO_CONTROL_PENDANT
#include <proficio/bimanual_robot.h>    // NOLINT(build/include_order)


using barrett::systems::connect;
BARRETT_UNITS_FIXED_SIZE_TYPEDEFS;


const char* remote_host = NULL;
double kp = 3e3;
double kd = 3e1;
bool game_exit = false;

bool validate_args(int argc, char** argv) {
  switch (argc) {
    case 4:
      kd = atof(argv[3]);
    case 3:
      kp = atof(argv[2]);
    case 2:
      remote_host = argv[1];
      break;
    default:
      remote_host = "127.0.0.1";
      printf("Defaulting to 127.0.0.1\n");
  }
  printf("Gains: kp = %f; kd = %f\n", kp, kd);
  return true;
}

namespace cube_sphere_bimanual {
/** When killed from outside (by GUI), this allows a graceful exit. */
void exit_program_callback(int signum) { game_exit = true; }
}  // namespace cube_sphere_bimanual

cf_type scale(boost::tuple<cf_type, double> t) {
  return t.get<0>() * t.get<1>();
}

int main(int argc, char** argv) {
  BARRETT_UNITS_TEMPLATE_TYPEDEFS(3);

  if (!validate_args(argc, argv)) {
    return 1;
  }

  const int kLeftRobot = 1;
  const int kRightRobot = 2;

  // The BimanualWam class creates two instances of the Wam class with
  // associated ProductManagers. It needs to be initialized by calling the
  // init() function to start up the robots.
  proficio::BimanualRobot<3> robots;
  // This program assumes that the left robot is on port 0 and the right robot
  // is on port 1. There are some checks for this in the init() function, but
  // it is assumed that the user has run the bimanual_homecheck script before
  // running this demo.
  // Once hardware support for configuration detection is available, this will
  // not be necessary.
  robots.init("proficio_left/default.conf.0", "proficio_right/default.conf.1");
  // Wait for both robots to start. isReady(robotNum) returns true once that
  // robot is in ACTIVE state and sending position data.
  while (!robots.isReady(kLeftRobot) || !robots.isReady(kRightRobot)) {
    barrett::btsleep(0.001);
  }

  // set up left arm
  barrett::ProductManager *product_manager_left = robots.getPm(kLeftRobot);
  barrett::systems::Wam<3> *proficio_left = robots.getWam(kLeftRobot);

  // set up right arm
  barrett::ProductManager *product_manager_right = robots.getPm(kRightRobot);
  barrett::systems::Wam<3> *proficio_right = robots.getWam(kRightRobot);

  proficio_left->gravityCompensate();
  proficio_right->gravityCompensate();

  // Catch kill signals if possible for a graceful exit.
  signal(SIGINT, cube_sphere_bimanual::exit_program_callback);
  signal(SIGTERM, cube_sphere_bimanual::exit_program_callback);
  signal(SIGKILL, cube_sphere_bimanual::exit_program_callback);

  std::string filename_left = "calibration_data/wam3/LeftConfig.txt";
  std::string filename_right = "calibration_data/wam3/RightConfig.txt";

  proficio::systems::UserGravityCompensation<3> gravity_comp_left(
      barrett::EtcPathRelative(filename_left).c_str());
  gravity_comp_left.setGainZero();

  proficio::systems::UserGravityCompensation<3> gravity_comp_right(
      barrett::EtcPathRelative(filename_right).c_str());
  gravity_comp_right.setGainZero();

  // systems for communication with the python visualization
  NetworkHaptics<3> network_haptics_left(
      product_manager_left->getExecutionManager(),
      remote_host, &gravity_comp_left, 5555, 5556);
  NetworkHaptics<3> network_haptics_right(
      product_manager_right->getExecutionManager(),
      remote_host, &gravity_comp_right, 5557, 5558);

  // Need separate instances of each haptic object for each robot because each
  // system can only take one set of inputs and can only connect to one
  // execution manager.
  const cp_type kBallCenter(0.4, -0.15, 0.0);
  const double kBallRadius = 0.12;
  barrett::systems::HapticBall ball_left(kBallCenter, kBallRadius);
  barrett::systems::HapticBall ball_right(kBallCenter, kBallRadius);

  const cp_type kBoxCenter(0.35, 0.2, 0.0);
  const barrett::math::Vector<3>::type kBoxSize(0.2, 0.2, 0.2);
  barrett::systems::HapticBox box_left(kBoxCenter, kBoxSize);
  barrett::systems::HapticBox box_right(kBoxCenter, kBoxSize);

  // Adjust fault limits and torque saturation limit.
  barrett::SafetyModule* safety_module_left =
    product_manager_left->getSafetyModule();
  barrett::SafetyModule* safety_module_right =
    product_manager_right->getSafetyModule();
  safety_module_left->setVelocityLimit(1.5);
  safety_module_right->setVelocityLimit(1.5);
  safety_module_left->setTorqueLimit(3.0);
  safety_module_right->setTorqueLimit(3.0);

  const jt_type kJointTorqueLimits(35.0);
  v_type damping_constants(20.0);
  damping_constants[2] = 10.0;
  damping_constants[0] = 30.0;
  jv_type velocity_limits(1.4);
  const jv_type kJointVelFilterFreq(20.0);

  // systems for left arm
  barrett::systems::Constant<double> zero_left(0.0);
  barrett::systems::Summer<cf_type> direction_sum_left;
  barrett::systems::Summer<double> depth_sum_left;
  barrett::systems::PIDController<double, double> pid_controller_left;
  barrett::systems::TupleGrouper<cf_type, double> tuple_grouper_left;
  barrett::systems::Callback<boost::tuple<cf_type, double>, cf_type>
    multiplier_left(scale);
  barrett::systems::ToolForceToJointTorques<3> tf2jt_left;
  barrett::systems::Summer<jt_type, 3> joint_torque_sum_left("+++");
  proficio::systems::JointTorqueSaturation<3> joint_torque_saturation_left(
      kJointTorqueLimits);
  proficio::systems::JointVelocitySaturation<3> velsat_left(damping_constants,
                                                            velocity_limits);
  barrett::systems::FirstOrderFilter<jv_type> joint_vel_filter_left;
  joint_vel_filter_left.setLowPass(kJointVelFilterFreq);

  // systems for right arm
  barrett::systems::Constant<double> zero_right(0.0);
  barrett::systems::Summer<cf_type> direction_sum_right;
  barrett::systems::Summer<double> depth_sum_right;
  barrett::systems::PIDController<double, double> pid_controller_right;
  barrett::systems::TupleGrouper<cf_type, double> tuple_grouper_right;
  barrett::systems::Callback<boost::tuple<cf_type, double>, cf_type>  // NOLINT
    multiplier_right(scale);
  barrett::systems::ToolForceToJointTorques<3> tf2jt_right;
  barrett::systems::Summer<jt_type, 3> joint_torque_sum_right("+++");
  proficio::systems::JointTorqueSaturation<3> joint_torque_saturation_right(
      kJointTorqueLimits);
  proficio::systems::JointVelocitySaturation<3> velsat_right(damping_constants,
                                                             velocity_limits);
  barrett::systems::FirstOrderFilter<jv_type> joint_vel_filter_right;
  joint_vel_filter_right.setLowPass(kJointVelFilterFreq);

  // set controller gains
  pid_controller_left.setKp(kp);
  pid_controller_left.setKd(kd);
  pid_controller_right.setKp(kp);
  pid_controller_right.setKd(kd);

  // ModXYZ systems are used for aligning coordinate frames. x and y axes
  // have their direction reversed in the python visualization, so they are
  // negated here for both position and force. For position, offsets are also
  // applied so the workspace of each robot corresponds roughly with the visual
  // workspace. The y offset is different because the two systems are mirrored.
  // The y position offset is chosen so that the two cursors appear in roughly
  // the same place with the robot endpoints are touching, assuming standard
  // separation distance (1100 mm) between the two robot centers.
  barrett::systems::modXYZ<cp_type> mod_axes_left;
  barrett::systems::modXYZ<cp_type> mod_axes_right;
  mod_axes_left.negX();
  mod_axes_left.negY();
  mod_axes_right.negX();
  mod_axes_right.negY();
  const cp_type kOffsetLeft(0.85, 0.13, -0.2);
  const cp_type kOffsetRight(kOffsetLeft[0],
                             -1.0*kOffsetLeft[1],
                             kOffsetLeft[2]);
  mod_axes_left.xOffset(kOffsetLeft[0]);
  mod_axes_left.yOffset(kOffsetLeft[1]);
  mod_axes_left.zOffset(kOffsetLeft[2]);
  mod_axes_right.xOffset(kOffsetRight[0]);
  mod_axes_right.yOffset(kOffsetRight[1]);
  mod_axes_right.zOffset(kOffsetRight[2]);

  barrett::systems::modXYZ<cf_type> mod_force_left;
  barrett::systems::modXYZ<cf_type> mod_force_right;
  mod_force_left.negX();
  mod_force_left.negY();
  mod_force_right.negX();
  mod_force_right.negY();

  // connect systems to set up the control loop

  // Robot position gets its coordinate axes transformed, then gets sent to
  // the visualization and the haptic object systems
  connect(proficio_left->toolPosition.output, mod_axes_left.input);
  connect(mod_axes_left.output, network_haptics_left.input);
  connect(mod_axes_left.output, ball_left.input);
  connect(mod_axes_left.output, box_left.input);

  connect(proficio_right->toolPosition.output, mod_axes_right.input);
  connect(mod_axes_right.output, network_haptics_right.input);
  connect(mod_axes_right.output, ball_right.input);
  connect(mod_axes_right.output, box_right.input);

  // Output of haptic object systems is summed. This method only works when
  // the cursor cannot touch multiple objects at the same time. It works here
  // because direction and depth outputs are equal to zero when the cursor is
  // not penetrating the object.
  connect(ball_left.directionOutput, direction_sum_left.getInput(0));
  connect(ball_left.depthOutput, depth_sum_left.getInput(0));
  connect(box_left.directionOutput, direction_sum_left.getInput(1));
  connect(box_left.depthOutput, depth_sum_left.getInput(1));

  connect(ball_right.directionOutput, direction_sum_right.getInput(0));
  connect(ball_right.depthOutput, depth_sum_right.getInput(0));
  connect(box_right.directionOutput, direction_sum_right.getInput(1));
  connect(box_right.depthOutput, depth_sum_right.getInput(1));

  // PID controller on the depth, trying to drive it to zero. Note again that
  // the depth is zero when the cursor is not touching an object, so the
  // feedback input is only nonzero when the cursor is penetrating the object.
  connect(depth_sum_left.output, pid_controller_left.referenceInput);
  connect(zero_left.output, pid_controller_left.feedbackInput);

  connect(depth_sum_right.output, pid_controller_right.referenceInput);
  connect(zero_right.output, pid_controller_right.feedbackInput);

  // Multiply the force and direction.The input to the "multiplier_" system is a
  // tuple of a vector and a double. The output is their product.
  connect(direction_sum_left.output, tuple_grouper_left.getInput<0>());
  connect(pid_controller_left.controlOutput, tuple_grouper_left.getInput<1>());
  connect(multiplier_left.output, mod_force_left.input);

  connect(direction_sum_right.output, tuple_grouper_right.getInput<0>());
  connect(pid_controller_right.controlOutput,
          tuple_grouper_right.getInput<1>());
  connect(multiplier_right.output, mod_force_right.input);

  // Transform the force vector back into the robot frame (negate the x and y
  // axes with a ModXYZ system defined above).
  connect(tuple_grouper_left.output, multiplier_left.input);
  connect(tuple_grouper_right.output, multiplier_right.input);

  // Calculate the joint torques necessary to apply the specified endpoint
  // force. This requires the robot kinematics.
  connect(proficio_left->kinematicsBase.kinOutput, tf2jt_left.kinInput);
  connect(mod_force_left.output, tf2jt_left.input);

  connect(proficio_right->kinematicsBase.kinOutput, tf2jt_right.kinInput);
  connect(mod_force_right.output, tf2jt_right.input);

  // Connect the user gravity compensation and the velocity saturation.
  connect(proficio_left->jpOutput, gravity_comp_left.input);
  connect(proficio_left->jvOutput, joint_vel_filter_left.input);
  connect(joint_vel_filter_left.output, velsat_left.input);
  connect(proficio_right->jpOutput, gravity_comp_right.input);
  connect(proficio_right->jvOutput, joint_vel_filter_right.input);
  connect(joint_vel_filter_right.output, velsat_right.input);

  // Sum the haptic torques with the additional torques from the user gravity
  // compensation and the velocity saturation.
  connect(tf2jt_left.output, joint_torque_sum_left.getInput(0));
  connect(gravity_comp_left.output, joint_torque_sum_left.getInput(1));
  connect(velsat_left.output, joint_torque_sum_left.getInput(2));

  connect(tf2jt_right.output, joint_torque_sum_right.getInput(0));
  connect(gravity_comp_right.output, joint_torque_sum_right.getInput(1));
  connect(velsat_right.output, joint_torque_sum_right.getInput(2));

  // Apply saturation limits.
  connect(joint_torque_sum_left.output, joint_torque_saturation_left.input);
  connect(joint_torque_sum_right.output, joint_torque_saturation_right.input);

  // Activate torques to robots.
  connect(joint_torque_saturation_left.output, proficio_left->input);
  connect(joint_torque_saturation_right.output, proficio_right->input);

  // create temporary file for startup script
  std::ofstream start_flag;
  start_flag.open("/tmp/csbstart");
  start_flag.close();

  // Run until either robot disconnects, then deactivate both robots.
  std::cout << "Entering main loop... shift-idle either robot to quit." <<
    std::endl;
  while (!robots.isShutdownComplete(1) &&
         !robots.isShutdownComplete(2) && !game_exit) {
    barrett::btsleepRT(0.001);
  }
  barrett::systems::disconnect(proficio_left->input);
  barrett::systems::disconnect(proficio_right->input);

  safety_module_left->setMode(barrett::SafetyModule::IDLE);
  safety_module_right->setMode(barrett::SafetyModule::IDLE);
  safety_module_left->waitForMode(barrett::SafetyModule::IDLE, false);
  safety_module_right->waitForMode(barrett::SafetyModule::IDLE, false);

  return 0;
}
