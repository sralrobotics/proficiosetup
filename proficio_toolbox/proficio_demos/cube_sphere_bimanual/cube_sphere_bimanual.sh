#!/bin/bash
#  Copyright 2016 Barrett Technology support@barrett.com
#
#  This file is part of proficio_toolbox.
#
#  This version of proficio_toolbox is free software: you can redistribute it
#  and/or modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This version of proficio_toolbox is distributed in the hope that it will be
#  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this version of proficio_toolbox.  If not, see
#  http://www.gnu.org/licenses/.

# @file cube_sphere_bimanual.sh
#
# This script runs the bimanual cube sphere demo, both the haptics program and
# the associated visualization.
#
# To prevent the user from running multiple instances at the same time, it
# creates a lock file in /tmp/ that allows it to detect when another similar
# script is already running. (Some other demo programs create a lock file with
# the same name. This is intentional to prevent trying to run two demos at the
# same time.)
#
# @TODO(ab): Make sure all the demos use the same lock file?
#
# For running this program from the Proficio GUI, another file is used as a
# startup flag.
#
# When either the haptics program or the visualization is ended, it ends the
# other automatically.
#
# NOTICE: This program is for demonstration purposes only. It is not approved
# for clinical use.

file=/tmp/csbstart
LOCK=/tmp/mylock
if [ -f $LOCK ]; then
  exit
fi
trap "rm -f $file $LOCK; exit" 0 1 2 3 6 8 9 14 15
touch $LOCK
cd ~/proficio_toolbox_wkp/proficio_demos/cube_sphere_bimanual
./cube_sphere_bimanual 127.0.0.1 &
PID1=$!
trap "rm -f $LOCK; rm -f $file; kill -9 $PID1; exit" 0 1 2 3 6 8 9 14 15
while [ ! -f "$file" ]; do
  sleep 1
done
python cube_sphere_bimanual_visualization.py 127.0.0.1 &
PID2=$!
trap "rm -f $LOCK; rm -f $file; kill -INT $PID1; kill -9 $PID2; exit" 0 1 2 3 6 8 9 14 15
while ps -p $PID1 > /dev/null && ps -p $PID2 > /dev/null; do
  sleep 0.5
done
if ps -p $PID1 > /dev/null
then
   kill -9 $PID1
elif ps -p $PID2 > /dev/null
then
   kill -9 $PID2
fi
rm $file $LOCK
