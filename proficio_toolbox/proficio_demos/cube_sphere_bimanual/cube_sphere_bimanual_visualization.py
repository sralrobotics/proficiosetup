#!/usr/bin/python
#  Copyright 2016 Barrett Technology support@barrett.com
#
#  This file is part of proficio_toolbox.
#
#  This version of proficio_toolbox is free software: you can redistribute it
#  and/or modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This version of proficio_toolbox is distributed in the hope that it will be
#  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this version of proficio_toolbox.  If not, see
#  http://www.gnu.org/licenses/.

"""
@file cube_sphere_bimanual_visualization.py

Visualization for cube_sphere_bimanual demo

NOTICE: This program is for demonstration purposes only. It is not approved
for clinical use.
"""

import sys
from socket import socket, AF_INET, SOCK_DGRAM
from time import sleep
import struct
from visual import *
import numpy


def get_remote_host():
    """ Returns IP address from command line argument. 
        Defaults to 127.0.0.1 """
    if len(sys.argv) == 2:
        return sys.argv[1]
    else:
        print("Defaulting to 127.0.0.1 for WAM IP Address")
        return "127.0.0.1"


def make_socket(remote_host, port_src, port_dest):
    """ Create and return a socket connected to remote_host
    @param remote_host      IP address
    @param port_src         source port
    @param port_dest        destination port """
    sock = socket(AF_INET, SOCK_DGRAM)
    sock.bind(('', port_src))
    sock.connect((remote_host, port_dest))
    return sock


def main():
    """ Creates a visual world with a cube and sphere.

    Uses sockets to send user gravity compensation settings and receive
    robot endpoint positions from two robots. """
    remote_host = get_remote_host()
    msg_format = "d" * 3  # messages contain 3 doubles
    msg_format_send = "d" * 4  # messages contain 4 doubles
    msg_size_send = struct.calcsize(msg_format_send)
    msg_size = struct.calcsize(msg_format)

    port_src1 = 5556
    port_src2 = 5558
    port_dest1 = 5555
    port_dest2 = 5557
    sock1 = make_socket(remote_host, port_src1, port_dest1)
    sock2 = make_socket(remote_host, port_src2, port_dest2)

    # set up visual
    scene.fullscreen = True
    #scene.cursor.visible = False
    #scene.autocenter = True
    #scene.autoscale = True
    scene.center = (0, -0.4, 1)
    scene.scale = (2.5, 2.5, 2.5)

    f = frame()
    f.rotate(angle=-pi / 2.0, axis=(1, 0, 0), origin=(0, 0, 0))
    f.rotate(angle=-pi / 2.0, axis=(0, 1, 0), origin=(0, 0, 0))
    f.rotate(angle=pi * 0.2, axis=(1, 0, 0), origin=(0, 0, 0))

    # unit vectors
    #arrow(pos = (0,0,0), axis = (1,0,0), color = color.red, frame = f)
    #arrow(pos = (0,0,0), axis = (0,1,0), color = color.green, frame = f)
    #arrow(pos = (0,0,0), axis = (0,0,1), color = color.blue, frame = f)

    # floor
    xMin, xMax = -1.0, 0.4
    yMin, yMax = -0.8, 0.8
    z = -0.5
    step = 0.1

    for x in numpy.linspace(xMin, xMax, (xMax - xMin) / step):
        curve(pos=[(x, yMin, z), (x, yMax, z)], frame=f)
    for y in numpy.linspace(yMin, yMax, (yMax - yMin) / step):
        curve(pos=[(xMin, y, z), (xMax, y, z)], frame=f)

    # haptic objects
    sphere(pos=(0.4, -0.15, 0), radius=0.11, color=color.green, opacity=0.6,
           frame=f)
    box(pos=(0.35, 0.2, 0), length=0.19, height=0.19, width=0.19,
        color=color.blue, opacity=0.6, frame=f)

    # end points
    ep1 = sphere(pos=(0, 0, 0), radius=0.01, color=color.red, frame=f)
    ep2 = sphere(pos=(0, 0, 0), radius=0.01, color=color.red, frame=f)

    grav_comp = 100
    while True:
        if scene.kb.keys:  # If any input was pressed
            s = scene.kb.getkey()
            if s == 'up':
                grav_comp = 1  # increase user gravity compensation
            elif s == 'down':
                grav_comp = 2  # decrease user gravity compensation
            elif s == 'delete':
                grav_comp = 0  # reset (turn off) user gravity compensation
            else:
                grav_comp = 100  # do nothing
            # Send once to each robot because each has its own user gravity
            # compensation system.
            sock1.send(struct.pack(msg_format_send, grav_comp, 1, 1, 1))
            sock2.send(struct.pack(msg_format_send, grav_comp, 1, 1, 1))
            sock1.send(struct.pack(msg_format_send, 100, 1, 1, 1))
            sock2.send(struct.pack(msg_format_send, 100, 1, 1, 1))
        ep1.pos = struct.unpack(msg_format, sock.recv(msg_size))
        ep2.pos = struct.unpack(msg_format, sock.recv(msg_size))
    sock1.close()
    sock2.close()


if __name__ == "__main__":
    main()
