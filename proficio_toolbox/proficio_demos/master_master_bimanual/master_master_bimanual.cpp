/*  Copyright 2016 Barrett Technology <support@barrett.com>
 *
 *  This file is part of proficio_toolbox.
 *
 *  This version of proficio_toolbox is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This version of proficio_toolbox is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this version of proficio_toolbox.  If not, see
 *  <http://www.gnu.org/licenses/>.
 */

/** @file master_master_bimanual.cpp
 *
 *  This is a demonstration of the capabilities of the Proficio robot for
 *  rehabilitation. In this demo, the two robots are linked in bilateral
 *  teleoperation (aka master-master control) in a mirrored relationship.
 *  When one robot is moved by an external force, the other robot is controlled
 *  to move to the mirror position.
 *
 *  In this demo, it is assumed that there is a left-configured Proficio
 *  connected to port 0 and a right-configured Proficio connected to port 1.
 *  It is recommended to run the bimanual_homecheck script before running this
 *  program to ensure that the robots are correctly connected and configured.
 *
 *  NOTICE: This program is for demonstration purposes only.
 *  It is not approved for clinical use.
 */

#include <barrett/log.h>
#include <proficio/systems/utilities/joint_torque_saturation.h>

#define NO_CONTROL_PENDANT
#include <proficio/bimanual_robot.h>

using barrett::systems::connect;

int main(int argc, char** argv) {
  BARRETT_UNITS_TEMPLATE_TYPEDEFS(3);

  // The BimanualWam class creates to instances of the Wam class with
  // associated ProductManagers. It needs to be initialized by calling the
  // init() function to start up the robots.
  proficio::BimanualRobot<3> robots;
  // This program assumes that the left robot is on port 0 and the right robot
  // is on port 1. There are some checks for this in the init() function, but
  // it is assumed that the user has run the bimanual_homecheck script before
  // running this demo.
  // Once hardware support for configuration detection is available, this will
  // not be necessary.
  robots.init("proficio_left/default.conf.0", "proficio_right/default.conf.1");
  // Wait for both robots to start. isReady(robotNum) returns true once that
  // robot is in ACTIVE state and sending position data.
  while (!robots.isReady(1) || !robots.isReady(2)) {
    barrett::btsleep(0.001);
  }

  const int kLeftRobot = 1;
  const int kRightRobot = 2;

  // set up left arm
  barrett::ProductManager *product_manager_left = robots.getPm(kLeftRobot);
  barrett::systems::Wam<3> *proficio_left = robots.getWam(kLeftRobot);
  barrett::systems::ExposedOutput<jp_type> *joint_position_left =
    robots.getJointPosSys(kLeftRobot);

  // set up right arm
  barrett::ProductManager *product_manager_right = robots.getPm(kRightRobot);
  barrett::systems::Wam<3> *proficio_right = robots.getWam(kRightRobot);
  barrett::systems::ExposedOutput<jp_type> *joint_position_right =
    robots.getJointPosSys(kRightRobot);

  proficio_left->gravityCompensate();
  proficio_right->gravityCompensate();

  // Set up jointspace master-master control (bilateral teleoperation). Since
  // the two robots are in opposite configurations, they will mirror each other.
  // This requires negating the second and third joints.
  barrett::systems::modXYZ<jp_type> reference_position_left;
  barrett::systems::modXYZ<jp_type> reference_position_right;
  reference_position_left.negY();
  reference_position_left.negZ();
  reference_position_right.negY();
  reference_position_right.negZ();
  connect(joint_position_right->output, reference_position_left.input);
  connect(joint_position_left->output, reference_position_right.input);
  barrett::systems::PIDController<jp_type, jt_type> pid_controller_left;
  barrett::systems::PIDController<jp_type, jt_type> pid_controller_right;
  connect(reference_position_left.output, pid_controller_left.referenceInput);
  connect(reference_position_right.output, pid_controller_right.referenceInput);
  connect(proficio_left->jpOutput, pid_controller_left.feedbackInput);
  connect(proficio_right->jpOutput, pid_controller_right.feedbackInput);
  jp_type kp, kd, ki;
  // @TODO(ab): Tune these gains. They can probably be stiffer.
  kp << 240, 200, 180;
  kd << 1, 1, 1;
  ki << 0, 0, 0;
  pid_controller_left.setKp(kp);
  pid_controller_left.setKd(kd);
  pid_controller_left.setKi(ki);
  pid_controller_right.setKp(kp);
  pid_controller_right.setKd(kd);
  pid_controller_right.setKi(ki);

  // @TODO(ab): Add user gravity compensation. What will be the keyboard
  // interface here?

  // Saturate joint torques
  const jt_type kJointTorqueLimits(35.0);
  proficio::systems::JointTorqueSaturation<3> joint_torque_saturation_left(
      kJointTorqueLimits);
  proficio::systems::JointTorqueSaturation<3> joint_torque_saturation_right(
      kJointTorqueLimits);
  connect(pid_controller_left.controlOutput,
          joint_torque_saturation_left.input);
  connect(pid_controller_right.controlOutput,
          joint_torque_saturation_right.input);

  printf("Press enter to move both robots to starting position.\n");
  barrett::detail::waitForEnter();
  proficio_left->moveHome();
  proficio_right->moveHome();

  printf("Press enter to begin master-master control.\n");
  barrett::detail::waitForEnter();

  // Release the hold position from moveHome()
  proficio_left->idle();
  proficio_right->idle();

  // Connect PID controllers for master-master control
  connect(joint_torque_saturation_left.output, proficio_left->input);
  connect(joint_torque_saturation_right.output, proficio_right->input);

  printf("Entering main loop... shift-idle either robot to quit.\n");
  while (!robots.isShutdownComplete(1) && !robots.isShutdownComplete(2)) {
    barrett::btsleep(0.001);
  }
  proficio_left->idle();
  proficio_right->idle();

  robots.quit();

  // Wait for idle mode before ending program so robots shut down properly
  product_manager_left->getSafetyModule()->waitForMode(
      barrett::SafetyModule::IDLE, false);
  product_manager_right->getSafetyModule()->waitForMode(
      barrett::SafetyModule::IDLE, false);
  return 0;
}
