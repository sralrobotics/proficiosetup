#!/bin/bash
#  Copyright 2016 Barrett Technology support@barrett.com
#
#  This file is part of proficio_toolbox.
#
#  This version of proficio_toolbox is free software: you can redistribute it
#  and/or modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This version of proficio_toolbox is distributed in the hope that it will be
#  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this version of proficio_toolbox.  If not, see
#  http://www.gnu.org/licenses/.

# @file bimanual_homecheck.sh
#
# This script uses the homecheck program to test for a left Proficio on port 0
# and a right Proficio on port 1. This setup is required for the current
# bimanual demos.
# 
# This will become unnecessary when hardware support for configuration
# detection is available.
#
# exit codes
#   0   success
#   113 left proficio homecheck fail
#   114 right proficio homecheck fail
#   115 another instance of this script is running
#   116 external kill signal received
#   117 config file not found
#
# NOTICE: This program is for demonstration purposes only. It is not approved
# for clinical use.

LOCK=/tmp/mylockhomecheck
exitcode=0

my_exit () {
  if [ -f ~/.barrett/default.conf.tmp ]; then
    mv ~/.barrett/default.conf.tmp ~/.barrett/default.conf
  elif [ -f ~/.barrett/default.conf.0 ]; then
    cp ~/.barrett/default.conf.0 ~/.barrett/default.conf
  fi
  if [ -f ~/.barrett/wam3.conf.tmp ]; then
    mv ~/.barrett/wam3.conf.tmp ~/.barrett/wam3.conf
  fi
  if [ -f ~/.barrett/calibration.conf.tmp ]; then
    mv ~/.barrett/calibration.conf.tmp ~/.barrett/calibration.conf
  fi
  if [ -d ~/.barrett/calibration_data.tmp ]; then
    mv ~/.barrett/calibration_data.tmp ~/.barrett/calibration_data
  fi
  rm -f $LOCK
  exit $exitcode
}

my_exit_external () {
  exitcode=116
  my_exit
}

# if lock file already exists, then another instance of
# this script is already running.
if [ -f $LOCK ]; then
  exit 115
fi

# otherwise, trap kill signals and create lock file.
trap my_exit 0
trap my_exit_external 1 2 3 6 8 9 14 15
touch $LOCK

# back up current configuration files
if [ -f ~/.barrett/default.conf ]; then
  mv ~/.barrett/default.conf ~/.barrett/default.conf.tmp
fi
if [ -f ~/.barrett/wam3.conf ]; then
  mv ~/.barrett/wam3.conf ~/.barrett/wam3.conf.tmp
fi
if [ -f ~/.barrett/calibration.conf ]; then
  mv ~/.barrett/calibration.conf ~/.barrett/calibration.conf.tmp
fi
if [ -d ~/.barrett/calibration_data ]; then
  mv ~/.barrett/calibration_data ~/.barrett/calibration_data.tmp
fi

# check for left proficio configuration on port 0
if [ -d ~/.barrett/proficio_left ]; then # TODO(ab): check for all the necessary files in the directory?
  cp -r ~/.barrett/proficio_left/* ~/.barrett/
else
  notify-send "ERROR: Config file not found. (117)"
  exitcode=117
  exit
fi
if [ -f ~/.barrett/default.conf.0 ]; then
  cp ~/.barrett/default.conf.0 ~/.barrett/default.conf
else
  notify-send "ERROR: Config file not found. (117)"
  exitcode=117
  exit
fi
homecheck left
# homecheck returns 1 for success
if [ "$?" != "1" ]
then
  notify-send "ERROR: Left configuration check failed. Make sure left Proficio is connected to port 0 and placed in extended position. (113)"
  exitcode=113
  exit
else
  echo "Left Proficio configuration check successful."
fi

# check for right proficio configuration on port 1.
if [ -d ~/.barrett/proficio_right ]; then # TODO(ab): check for all the necessary files in the directory?
  cp -r ~/.barrett/proficio_right/* ~/.barrett/
else
  notify-send "ERROR: Config file not found. (117)"
  exitcode=117
  exit
fi
if [ -f ~/.barrett/default.conf.1 ]; then
  cp ~/.barrett/default.conf.1 ~/.barrett/default.conf
else
  notify-send "ERROR: Config file not found. (117)"
  exitcode=117
  exit
fi
homecheck right
# homecheck returns 1 for success
if [ "$?" != "1" ]
then
  notify-send "ERROR: Right configuration check failed. Make sure right Proficio is connected to port 1 and placed in extended position. (114)"
  exitcode=114
  exit
else
  echo "Right Proficio configuration check complete."
fi

exitcode=0
