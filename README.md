# Proficio PC Setup Instructions

This describes how to setup **from scratch** (clean wipe) for the proficio robot on a Ubuntu system. 
For regular use of the existing system, see the  <br>
    <LI> speed pilot reposityry <Br>
    or the 
    <LI> speed multi-day repository
    
You may need to ask Patton or Abdel Majeed for access
<>       


#### Version History:
1. v1: 05-07-2018 by Eyad Hajissa
2. v2: 03-04-2019 by Eyad 
3. v3: 10-20-2019 by Yaz Abdel Majeed <yabdelm2@gmail.com> 
4. v3.1: 10-30-2019 by Yaz
5. v4: 10-31-2019 by Yaz
6. v5: 12-23-2019 by Patton <pattonj@uic.edu>

### Note
In this document, command like `this` or
```shell script
this
```
are to be used in a terminal

### Step 1: Install Ubuntu & Kernel
##### To get ISO file, go to:
[Barrett Website Link](http://support.barrett.com/wiki/BuildingAPC/14-04-1_64_Install)

##### Next, use windows to create bootable disk
[Rufus Disk Utility](https://rufus.ie/)

##### Rufus Settings:
1. Select Ubuntu 14.04 ISO
2. Partition scheme: MBR
3. Target system: BIOS or UEFI
4. File system: FAT32 or FAT
5. Hit Start
6. Install ubuntu as usual, these instructions assume you create the following user:
    * username: robot
    * password: proficio

##### Get custom kernel:
```shell script
sudo wget http://web.barrett.com/support/WAM_Installer/linux-image-3.14.17-xenomai-2.6.4.barrett_amd64.deb
sudo wget http://web.barrett.com/support/WAM_Installer/linux-headers-3.14.17-xenomai-2.6.4.barrett_amd64.deb
sudo dpkg -i linux-image-3.14.17-xenomai-2.6.4.barrett_amd64.deb
sudo dpkg -i linux-headers-3.14.17-xenomai-2.6.4.barrett_amd64.deb
```
* Note: if the last command returns an error, do the following:
    1. Start nautilus as sudo: `sudo nautilus`
    2. Go to /lib/modules
    3. Create a copy of the xenomai folder (add `.old` to the copy) 
    4. Rename original folder to xenomai-2.6.4 (remove the .1) and try the last command again
    5. Add back the .1 to the folder name after running the last `sudo dpkg` command

##### Install Xenomai
```shell script
cd /usr/src
sudo wget http://web.barrett.com/support/WAM_Installer/xenomai-2.6.4.tar.bz2
sudo tar -xjf xenomai-2.6.4.tar.bz2
sudo rm xenomai-2.6.4.tar.bz2
```

##### Fix GRUB Boot
```shell script
sudo update-initramfs -c -k "3.14.17-xenomai-2.6.4.1"
sudo addgroup xenomai
sudo usermod -aG xenomai robot
XENGRPNUM=$(egrep -i "^xenomai" /etc/group | cut -d':' -f 3)
sudo sed -i "s/quiet\ splash/quiet\ splash\ xeno_nucleus.xenomai_gid=$XENGRPNUM/" /etc/default/grub
sudo sed -i '/exit\ 0/ i\/usr/xenomai/sbin/rtcanconfig\ rtcan0\ -b\ 1000000\ -c\ none\ start' /etc/rc.local
sudo sed -i '/exit\ 0/ i\/usr/xenomai/sbin/rtcanconfig\ rtcan1\ -b\ 1000000\ -c\ none\ start' /etc/rc.local
sudo update-grub
sudo add-apt-repository -y ppa:danielrichter2007/grub-customizer
sudo apt-get update
sudo apt-get install -y grub-customizer
```

##### Run GRUB Customizer
1. `sudo grub-customizer`
2. move kernel 3.14.17-xenomai-2.6.4 to the top and save

##### Build Xenomai userspace libs
```shell script
cd /usr/src/xenomai-2.6.4
sudo ./configure --enable-dlopen-skins
sudo make
sudo make install
sudo chgrp xenomai /dev/rtheap
sudo chgrp xenomai /dev/rtp*
sudo shutdown -r now
```

* Note: The last command is important, it restarts the computer and makes the xenomai realtime kernel default.

##### Install NVIDIA driver
1. Go to **Software & Updates** under System Settings
2. Under **Additional Drivers**, enable NVIDIA Proprietary driver version 340.107
3. Let it update and install the driver, you may need to restart.
4. Pin **NVIDIA X Server Settings** to the launcher for easy access
5. In the settings, make sure that the TV screen has **Orientation** set to **Invert** and **Reflect Along X**
* Note: Step 5 here may need to be repeated every time the system is restarted, which is why we pinned it

 ### Step 2: Install Barrett Software and Drivers
 
 ##### Install dependencies:
 ```shell script
sudo apt-get update
sudo apt-get install -y g++ cmake libncurses5-dev spell subversion ssh python-dev python-argparse libeigen3-dev libboost1.46 libgsl0-dev libxenomai-dev libboost-all-dev libboost-thread-dev libboost-python-dev python-visual libgtkglextmm-x11-1.2-dev git
wget http://web.barrett.com/support/WAM_Installer/libconfig-barrett_1.4.5-1_amd64.deb
sudo dpkg -i libconfig-barrett_1.4.5-1_amd64.deb
wget http://web.barrett.com/support/WAM_Installer/libconfig-1.4.5-PATCHED.tar.gz
tar -xf libconfig-1.4.5-PATCHED.tar.gz
cd libconfig-1.4.5
./configure && make && sudo make install
cd ../
rm -rf libconfig-1.4.5 libconfig-1.4.5-PATCHED.tar.gz
```

##### Install Libbarrett
* Note: The latest version of libbarrett to work with Proficio is 1.3, the zip file is included in the root folder of this repository
* Extract the folder into your home folder (so the files are in /home/robot/libbarrett)
* If you face issues with the folder in this repo, get release 1.3.0 from their [github](https://git.barrett.com/software/libbarrett), branch: **release/release-1.3.0**
```shell script
cd ~/libbarrett
cmake .
make
sudo make install
mv .bash_aliases ../
cd
. ~/.bashrc
```

##### Xenomai bash aliases
```shell script
sudo bash
echo /usr/xenomai/lib/ | cat > /etc/ld.so.conf.d/xenomai.conf
ldconfig
exit
git clone -b proficio_p3-svn https://git.barrett.com/software/btclient.git ~/btclient
cd ~/btclient
sh makeall
mv .bash_aliases ../
cd
. ~/.bashrc
```

##### To make it easier to open terminal from any folder:
```shell script
sudo apt-get install nautilus-open-terminal
```

### Step 3: Install Proficio Toolbox
- This is an extension to libbarrett that is specific to the Proficio
- It includes libbarrett-based systems, tools, and demos
- These are not guaranteed to run on any other robot other than Proficio
- For support, you can email support@barrett.com

##### Steps:
1. `sudo apt-get install libcgal-dev`
2. copy proficio_toolbox folder from this repository to your home directory
    * **Untested:** can clone toolbox from git: `git clone git@git.barrett.com:software/proficio_toolbox.git`
3. Compile and install:
```shell script
cd proficio_toolbox
mkdir build
cd build
sudo make clean
cmake ../
make
sudo make install
```
* Note: the second command in step 3 here may fail if folder already exists, 
that is not an issue, just continue with the rest of the commands

### Step 4: Gravity Calibration
* **VERY IMPORTANT:** If the robot has its original forearm attachment, skip to "calibration steps"
* Assuming the robot has the black forearm attachment with the ball joint,
copy the following folders from the repo and overwrite the files in the home folder:
    * .barrett
    * btclient
    * libbarrett_examples
    * libbarrett_sandbox
    * proficio_toolbox
##### Calibration steps
1. If Proficio is in the right configuration: `rightConfig`, else `leftConfig`
2. `homecheck right` or `homecheck left` as appropriate
3. Power cycle (turn it off for about a minute, then back on)
4. `bt-wam-gravitycal`
5. copy the gravitycal.conf to ~/proficio_toolbox/
6. If you want to run a demo:
```shell script
cd ~/proficio_toolbox/proficio_demos/cube_sphere
./cube_sphere
```

### Step 5: H3D
1. Extract the H3D.zip in this repo to the home folder
2. Install dependencies:
    ```shell script
    sudo apt-get install cmake freeglut3-dev libxerces-c2-dev libfreeimage-dev zlib1g-dev gcc g++ libglew-dev ftgl-dev libalut0 libalut-dev libopenal-dev libglew-dev libcurl4-openssl-dev libaudiofile-dev libfontconfig1-dev libfontconfig1 libvorbis-dev python-dev python-all-dev libxi-dev libxmu-dev python-wxglade libwxgtk2.8-dev
    ```
3. Build and install H3D components:
   ```shell script
   cd ~/H3D/H3DUTIL/build 
   sudo make clean
   sudo cmake .
   sudo make
   sudo make install
   cd ~/H3D/HAPI/build
   sudo make clean
   sudo cmake .
   sudo make
   sudo make install
   cd ~/HAPI/H3DAPI/build
   sudo make clean
   sudo cmake .
   sudo make
   sudo make install
   cd ~/HAPI/H3DAPI/H3DLoad/build
   sudo make clean
   sudo cmake .
   sudo make
   sudo make install
   cd ~/HAPI/H3DAPI/H3DViewer/build
   sudo make clean
   sudo cmake .
   sudo make
   sudo make install
   ```
   * Note: `make install` definitely needs `sudo`, but try the others without `sudo` first
   * Note 2: if you get errors with including python.h, just go to the lines where the errors happen (different files) and change: <Python.h> to <python2.7/Python.h>, also works for other “not-found” header files
4. Open a new terminal (Ctrl+Alt+T)
5. `sudo -H gedit /etc/environment`
6. **Append** the file with `H3D_ROOT="/home/robot/H3D/H3DAPI"`
7. Save te file and logout/log in again to apply changes
8. Set up terminal aliases for H3D commands:
    1. Ctrl+h to reveal hidden files
    2. Open the file .bashrc
    3. at the end paste:
        ```shell script
        #aliases for H3D
        alias H3DSettings='cd ~/H3D/H3DAPI/settings/; python SettingsGUI.py'
        alias H3DSphere='cd ~/H3D/H3DAPI/examples/All; H3DLoad --deviceinfo=BarrettDevice Sphere.x3d'
        ```
   4. Save and close the file

### Step 6: Important Software
##### Anaconda (Note: In H3D programs, this can ONLY be used to run the GUI and pycharm scripts)
1. Install anaconda from anaconda site, currenty [here](https://www.anaconda.com/distribution/)
2. Recommendation: install the python 3 anaconda distribution (currently 3.7):
    ```shell script
    cd ~/Downloads/
    sudo ./[anaconda file name]
    ```
3. **Important:** when it asks to add Anaconda to your path (~/.bashrc), select YES
4. Finally, to be able to use conda commands in the linux terminal: `source ~/.bashrc`
5. Open `anaconda-navigator` from a terminal (pin to launcher for easier access)
* Note: Next you can use the following steps to create a new environment, OR the steps to restore from file in this repo
##### Create New Conda Python 2.7 Environment
1. I normally call it py27, open a terminal from inside anaconda-navigator):
    ```shell script
    conda update conda
    conda create -n py27 python=2.7 anaconda
    ```
2. Activate py27, either using `conda activate py27`, `activate py27` or `source activate py27`
3. Install important packages:
    ```shell script
    conda install pandas numpy scipy scikit-learn matplotlib pyqt csv 
    ```
##### Restore Evironment from repo backup
1. copy py27_env.yml to the home folder
2. `cd ~/`
3. `conda env create -f py27_env.yml`
##### Extra: Back up current envrironment (for future reference)
1. Make sure the environment is active
2. `conda env export > ~/[environment-name].yml`
##### Python (Note: This is separate from anaconda, HAS TO BE USED to run H3D)
1. Open a linux regular terminal, if you see an anaconda environment in () before your user, example: 
    ```shell script
    (base) robot@robot-pc$
    ```
    then type: `conda deactivate`
2. Install python dependencies:
    ```shell script
    sudo apt-get install python-pip
    sudo apt-get install python3-pip
    sudo apt-get install build-essential checkinstall
    sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
    ```
3. Install important python packages:
    ```shell script
    sudo apt-get install python-numpy
    sudo apt-get install python3-numpy
    sudo apt-get install python-pandas
    sudo apt-get install python3-pandas
    sudo apt install python-sklearn
    sudo apt install python3-sklearn
    ```

##### GitKraken

Only install from the deb file in this repo, it is the latest version supported on Ubuntu 14.04. **Never Update!!**

### Extra: Scaling small monitor resolution
The current computer screen connected to the system has a resolution of
1600x900, which means not a lot can fit on screen, to resolve can change screen
scaling:
```shell script
xrandr --output DVI-I-1 --scale 1.2x1.2
```
where `DVI-I-1` is the ID of the computer monitor when loading the xrandr command,
this causes the acer monitor to go to 1920x1080. Finally, go into NVIDIA settings
(shortcut in unity bar) and drag screens around so they don't overlap.
* **Note from Yaz**: I used to do this, but it was too much trouble with every restart, including here for reference
#### This should end the setup, just sync the repo with experiment code and follow the instructions there

# The End

***

#### Under this heading, I'm including optional and currently unused instructions, in case they come in handy for troubleshooting purposes

##### 1. Running H3DLoad with the proficio
The --deviceinfo always has to be specified to make the Proficio work as the haptic device as 	follows:
	
	H3DLoad –deviceinfo=/home/robot/H3D/H3DAPI/settings/common/device/BarrettDevice [x3dfilename or path+filename]
	

##### 2. Install the leap
1. download the latest leap sdk for linux at leapmotion.com (should be under sdk or developers)
2. extract the downloaded directory to your home folder
3. README file contains instructions
4. `sudo apt-get install mesa-common-dev-lts-trusty` (or mesa package for your version of linux)
5. double click on the .deb package and let it install in the software center
6. plug in the leap and run `LeapControlPanel`
7. right click the leap icon on the desktop tray, click settings
8. Under troubleshooting, start diangnostics, wave hands to get all green
9. Set up python system paths to be able to import the Leap library form the SDK when “import Leap” is invoked in a python terminal or script
10. In a new terminal:
    ```python
    python
    import sys
    sys.path.insert(0,"/home/robot/LeapDeveloperKit_2.3.1+31549_linux/LeapSDK/lib")
    sys.path.insert(0,"/home/robot/LeapDeveloperKit_2.3.1+31549_linux/LeapSDK/lib/x64")
    import Leap
    ```

##### 3. Graphics Setup (currently unused, not needed)
you may want to use a LookingGlass as a screen while working on ubuntu in which case you need to flip your display to be able to see it in the LookingGlass mirror.
1. Go to display and rotate the display 180 degrees
2. in a new terminal type: xrandr -x
This works with 3rd party drivers on nvidia graphics cards but not with nvidia drivers. https://askubuntu.com/questions/19936/how-can-i-mirror-flip-display-output

A new legacy Nvidia driver is also available.

updated driver causes symbolic link error when compiling HAPI but the following link update solves the problem:
```shell script
cd /usr/lib/x86_64-linux-gnu/
sudo ln -s /usr/lib/libGL.so.340.107 libGL.so
```

##### 4. Python solutions (defunct or unused)
1. Tkinter: `sudo apt-get install python-tk`
2. If you get this error:
```shell script
ImportError: <module 'setuptools.dist' from '/usr/lib/python2.7/dist-	packages/setuptools/dist.pyc'> has no 'check_specifier' attribute
```
This means you're running an older version of pip setuptools, to resolve:
```shell script
sudo pip install setuptools --upgrade
```